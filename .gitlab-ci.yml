image: python:3.10-slim

stages:
  - Static Analysis
  - Test
  # Separate FP and model generate stages, so the FP job isn't
  # overwhelmed by the 3D model (manual) jobs.
  - Footprint Generate
  - 3D Generate

variables:
  # By default, use the master branch of the main KLU repository
  # You can override this in your projects GitLab UI
  KICAD_LIBRARY_UTILS_REPO: https://gitlab.com/kicad/libraries/kicad-library-utils.git
  KICAD_LIBRARY_UTILS_BRANCH: master

fp-formatting:
  stage: Static Analysis
  needs: []
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  before_script:
    - python --version
    - python -m pip install --upgrade pip
    - pip install .[dev]
  script:
    - ./manage.sh fp_format_check

fp-test:
  stage: Test
  needs: []
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - gitlabci/**/*
        - .gitlab-ci.yml
        - src/**/*
        - KicadModTree/**/*
        - scripts/**/*
  before_script:
    - python --version
    - python -m pip install --upgrade pip
    - pip install '.[dev]'
  script:
    - python -m pytest --junitxml=report.xml
  artifacts:
    when: always
    reports:
      junit: report.xml

fp-generate:
  stage: Footprint Generate
  needs: []
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  image: python:3.10  # some scripts require more recent Python features
  before_script:
    - python --version
    - python -m pip install --upgrade pip
    - pip install .
  script:
    # Generate footprints
    - cd scripts
    - ls -lah
    - ./generate.sh
    # Collect generated footprints into artifact archive
    - cd "$CI_PROJECT_DIR"
    - gitlabci/collect_generated_footprints.sh
    - mv generated_footprints/ footprints/
    - tar -cJf footprints.tar.xz footprints/

    # Write out the generation results for metric reporting
    - ./gitlabci/library_metrics.sh "footprints" "metrics.txt"
  artifacts:
    paths:
      - footprints.tar.xz
    reports:
      metrics: metrics.txt
    when: always

fp-generate-and-compare:
  stage: Test
  needs: []
  tags:
    - saas-linux-medium-amd64
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - gitlabci/**/*
        - .gitlab-ci.yml
        - src/**/*
        - KicadModTree/**/*
        - scripts/**/*
        - data/**/*
  allow_failure: true
  image: kicad/kicad:8.0
  before_script:
    # Required when using kicad Docker image (kicad-cli needed for visu diff):
    - sudo apt update -qq
    - sudo apt install curl python3 python3-pip -qqy --no-install-recommends
    - sudo ln -s /usr/bin/python3 /usr/bin/python
    - python --version
    - python -m pip install --break-system-packages --upgrade pip
    - pip install --break-system-packages .

    - echo "Cloning ${KICAD_LIBRARY_UTILS_REPO} branch ${KICAD_LIBRARY_UTILS_BRANCH}"
    - git clone --depth 1 "${KICAD_LIBRARY_UTILS_REPO}" $CI_BUILDS_DIR/kicad-library-utils --branch "${KICAD_LIBRARY_UTILS_BRANCH}"
    - git config --global --add safe.directory $(realpath .)
    - export KICAD_LIBRARY_UTILS=$CI_BUILDS_DIR/kicad-library-utils
    - mkdir -p $HOME/.config/kicad/8.0/colors
    - pip install --break-system-packages --no-warn-script-location
        pygments
        wsdiff
        jinja2
    # Get the MR SHA values
    - source ${KICAD_LIBRARY_UTILS}/tools/gitlabci/common.sh
  script:
    # Download and extract base footprints generated by base branch (artifact)
    - BASE_FP_URL="$CI_API_V4_URL/projects/$CI_MERGE_REQUEST_PROJECT_ID/jobs/artifacts/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME/raw/footprints.tar.xz?job=fp-generate"
    - echo "Will try to download base branch footprints from $BASE_FP_URL"
    - curl -fL -o footprints.tar.xz "$BASE_FP_URL"
    - tar -xJf footprints.tar.xz
    - mv footprints/ base_footprints/
    # Generate footprints
    - cd scripts
    - ls -lah
    - time ./generate.sh
    # Collect them into generated_footprints/
    - cd "$CI_PROJECT_DIR"
    - gitlabci/collect_generated_footprints.sh
    # Hack: Ensure at least an empty dir exists in base_footprints/ for each
    # dir in generated_footprints/ (otherwise html_diff.py tries to interpret
    # paths of the former as Git revisions when new footprint libs are added)
    # (=> TODO: Add CLI arg to html_diff.py to force interpretation as dir?)
    - |
      shopt -s globstar nullglob
      for dir_path in generated_footprints/**/; do
        mkdir -p "base_footprints/${dir_path#generated_footprints/}"
      done

    # Required due to bug (?) or thing I don't understand in html_diff.py
    # (TODO: Try to fix there?)
    - unset CI_MERGE_REQUEST_ID
    # Generate visual diff between base and generated (adapted from
    # kicad-library-utils/tools/gitlabci/gitlab-ci-footprints.yml)
    # (=> TODO: Refactor to extract common functionality?)
    - gitlabci/visual_diff.sh "base_footprints" "generated_footprints" "diffs"

    # Write out the generation results for metric reporting
    - ./gitlabci/library_metrics.sh "generated_footprints" "metrics.txt"
  artifacts:
    expose_as: "Visual Diff"
    paths:
      - "diffs/"
    reports:
      dotenv: deploy.env
      metrics: metrics.txt
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: $DYNAMIC_ENVIRONMENT_URL

# Include the 3d-model-generators pipelines
include: '3d-model-generators/.gitlab-ci.yml'
